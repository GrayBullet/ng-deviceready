# ng-deviceready
AngularJS Apache Cordova DeviceReady service.


## How to use
Download files.
  * [angular.js or angular.min.js](https://angularjs.org/)
  * [ng-cordova.js or ng-cordova.min.js](http://ngcordova.com/docs/install/)
  * [ng-deviceready.js](ng-deviceready.js)

`Take` button change enabled after [deviceready](http://cordova.apache.org/docs/en/7.x/cordova/events/events.html#deviceready) envent.

```html
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="Content-Security-Policy" content="default-src 'self' data: gap: https://ssl.gstatic.com 'unsafe-eval'; style-src 'self' 'unsafe-inline'; media-src *; img-src 'self' data: content:;">
  <meta name="format-detection" content="telephone=no">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
  <title>ng-deviceready</title>
</head>
<body>
<div ng-app="app">
  <my-camera></my-camera>
</div>
<script src="angular.js"></script>
<script src="ng-cordova.js"></script>
<script src="ng-deviceready.js"></script>
<script src="cordova.js"></script>
<script src="main.js"></script>
</body>
</html>
```

```javascript
// main.js

angular.module('app', ['ngCordova', 'deviceready'])
  .component('myCamera', {
    controller: function ($cordovaCamera, deviceReady) {
      var that = this;

      // Until deviceready fired, button is disabled.
      that.disabled = function () {
        return !deviceReady.isDeviceReady;
      };

      that.take = function () {
        var options = {
          sourceType: Camera.PictureSourceType.CAMERA,
          destinationType: Camera.DestinationType.DATA_URL
        };

        $cordovaCamera.getPicture(options)
          .then(function (image) {
            that.image = 'data:image/jpeg;base64,' + image;
          });
      }
    },
    template: '<div>\n' +
    '<button ng-disabled="$ctrl.disabled()" ng-click="$ctrl.take()">take</button>\n' +
    '<img src="{{$ctrl.image}}">\n' +
    '</div>\n'
  });
```

## Reference
### deviceReady.isDeviceReady
Gets whether [deviceready](http://cordova.apache.org/docs/en/7.x/cordova/events/events.html#deviceready) event fired or not.


## License
[MIT](./LICENSE)
